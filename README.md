# Zvonky
Dashboard for [Zonky](https://www.zonky.com).

## Motivation
Main reason behind [PERLUR Zvonky](https://zvonky.perlur.cloud) is that both great tools I use - [RoboZonky](https://github.com/RoboZonky/robozonky) and [Stonky](https://www.facebook.com/groups/204405453512737/) are state-less, which limits their capabilities greatly; thus I decided to create Zvonky - combination of [Stonky](https://www.facebook.com/groups/204405453512737/), [Zotify](https://github.com/RoboZonky/robozonky) and [RoboZonky](https://github.com/RoboZonky/robozonky).
